package mvc;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Entry {

   private String date;
   private Integer count;

}
