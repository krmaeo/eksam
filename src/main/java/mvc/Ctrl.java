package mvc;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Ctrl {

    @PostMapping("/transform")
    public List<String> root(@RequestBody List<Entry> entries) {
        return getValidEntryDates(entries);
    }

    private List<String> getValidEntryDates(List<Entry> entries) {
        List<String> validDates = new ArrayList<>();
        for (Entry entry : entries) {
            if (entry.getDate() != null && entry.getCount() != null) {
                if (entry.getCount() > 1 && !validDates.contains(entry.getDate())) {
                    validDates.add(entry.getDate());
                }
            }
        }
        return validDates;
    }

}