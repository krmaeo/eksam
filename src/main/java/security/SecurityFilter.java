package security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SecurityFilter extends AbstractAuthenticationProcessingFilter {

    public SecurityFilter(AuthenticationManager authenticationManager, String url) {
        super(url);

        setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws IOException {

        // Read info from HttpServletRequest.
        String userName = "";
        String password = "";

        String json = request.getReader().lines().collect(Collectors.joining("\n"));

        String[] userCredentials = json.split("&");
        for (String credentialLine : userCredentials) {
            List<String> a = Arrays.asList(credentialLine.split("="));
            if (a.size() == 2) {
                if (a.get(0).equals("user")) {
                    userName = a.get(1);
                } else if (a.get(0).equals("pass")) {
                    password = a.get(1);
                }
            }
        }

        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(
                        userName,
                        password);

        return getAuthenticationManager().authenticate(token);
    }
}
