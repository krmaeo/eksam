Teooriaküsimused

  Teooriaküsimustele saate vastata 15 minuti jooksul alates eksami algusest (13:00).
  Muudatused, mis on tehtud hiljem kui 13:15 ei lähe arvesse.

  Vastused kirjutage siia samasse faili küsimuse alla.
  Iga küsimuse vastus peab jääma alla 200 märgi.
  Iga küsimus annab kuni 4 punkti.

  1. Miks piisab Gradle konfiguratsioonis mõnede teekide puhul "compileOnly" skoobist?

  CompileOnly tähendab seda, et seda teeki kasutatakse ainult rakendust käivitades kompileerimise ajal, hiljem ei kasutata ning põhjuseks on lihtne, seda teeki polegi tarvis peale kompileerimist.
  Üks näide oleks näiteks lombok. Lomboki annotatsioone kasutatakse ainult kompileerimisel. Kogu töö tehakse sellel ajal ära.

  2. Mis on sõltuvuste süstimise (Dependency Injection) tehnika eesmärk?

  Selle eesmärgid on: võimalus kasutada igast klassist ainult ühte loodud objekti koodi jooksutamise jooksul (et igal pool kasutatakse klassist sama instantsi, mitte ei looda uuesti).
  Samuti säästab see arendajat mingit objekti luues sellest, et ta peaks kasutama new... ning säästab kasutajat ise loomaks objekte objektile, millest ta sõltub. Kiirendab arendusprotsessi.

  3. Sql lausete parameetrite määramine stringe kokku liites võib võimaldada
     Sql Injection rünnakut. Nimetage mõni teine probleem, miks see lähenemine hea pole.

  See lähenemine ei anna koodis täit ülevaadet sql lausest. Kui kasutada näiteks sqlBuilderit, siis on koodis hea ülevaae, milline sql käsk nüüd kokku pannakse. Stringide liitmisel jääb kood väga segaseks ning pikaks ja koledaks.

  4. Projektis oli määratud "hibernate.hbm2ddl.auto" väärtuseks "validate".
     Mida valideeritakse? Miks?

  Valideeritakse koodi annotatsioonide vastavust olemasolevale andmebaasi skeemile. Et kõik @Entity, @Column ja sellised oleks õigete klasside peal ja kõik vastaks andmebaasi skeemile.
  Valideeritakse, et vältida märkamatuid vigu arendusprotsessis. See kindlustab, et käivitatud koodi objektid vastavad 100% ka loodud andmebaasi skeemile ja kõik toimib.

Ülesanne 1 (17 punkti)

  Kirjutage Spring Mvc rakendus, mis teeb järgmist.

  Postitades aadressile /transform Json-i kujul

  [ { "date": "17.12", "count": 5 },
    { "date": "21.12", "count": 8 },
    { "date": "23.12", "count": 3 },
    { "date": "18.12" },
    { "date": "18.12", "count": 1 },
    { "date": "23.12", "count": 9 }]

  väljastab rakendus need kuupäevad, mille "count" on üle ühe.

  ["17.12","21.12","23.12"]

  Korduvad kuupäevad tuleb eemaldada ja esialgne järjekord peab säilima.

  Kogu vajalik kood läheb klassidesse mvc.Ctrl ja mvc.Entry.

  Kood peab läbima testi klassist test.MvcTest.

Ülesanne 2 (17 punkti)

  Konfigureerige Spring Security nii, et rakendusel oleks sisselogimise võimalus.

  Postitades aadressile /login sisendi kujul "user=<kasutajanimi>&pass=<salasõna>".
  peaks kasutaja sisse logitama, kui salasõna on õige (määratud klassis security.SecurityConfig).

  Muuta võite kõiki klasse paketis "security". Uusi klasse luua ei tohi.

  Rakendus peab läbima testid klassist test.SecurityTest.
